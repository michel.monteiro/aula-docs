# Exemplo | EJCM

ExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemplo
ExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemploExemplo

 
**Status do Projeto**: Em período de suporte.

![Badge](https://img.shields.io/badge/Ionic-1572B6?style=for-the-badge&logo=ionic&logoColor=white)
![Badge](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![Badge](https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white)
![Badge](https://img.shields.io/badge/MySQL-316192?style=for-the-badge&logo=mysql&logoColor=white)
![Badge](https://img.shields.io/badge/Ethereum-454a75?style=for-the-badge&logo=Ethereum&logoColor=white)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white) 
![Badge](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge&logo=express&logoColor=white)
 
## Tabela de Conteúdo

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 5. [Arquitetura](#arquitetura)
 6. [Autores](#autores)
 
## Tecnologias utilizadas

**App Mobile:**
 - [Ionic](https://ionicframework.com/docs/intro/cli), *versão ^5.5.1*

 **Sistema web de administração:**
 - [Angular](https://angular.io/start), *versão ^10.0.6*

 **Backend:**
 - [Laravel](https://laravel.com/docs/8.x/installation), *versão ^7.24*
 - [Express](https://devdocs.io/express/), *versão ^4.17.1*
 - [Node.Js](https://nodejs.org/en/download/), *versão ^12.18.2*

**Linguagens:**
- [Typescript](https://www.typescriptlang.org/docs/), *versão ^3.9.5*
- Javascript
- [PHP](https://www.php.net/docs.php), *versão ^7.2.5*
- [Solidity](https://docs.soliditylang.org/en/v0.7.5/), *versão ^0.7.5*

 **Outros requisitos mínimos de software:**
- Node Pack Manager (npm), *versão ^5.0.0
- [Composer](https://getcomposer.org/download/) (gerenciador de pacotes PHP), *versão ^2.0*
- Servidor web (a exemplo do Apache2)

## Instalação
Após clonar este projeto em seu computador e tendo todos os pré-requisitos instalados, faremos o setup em cada uma das pastas.

**Frontend Mobile (*pasta 'frontend'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ npm install
```

**Frontend Web (*pasta 'adminWeb'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ npm install
```

**Backend PHP (*pasta 'backend'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ composer install
```

**Backend Node (*pasta 'ethereum'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ npm install
```

## Configuração
Neste tópico, será apenas necessário fazer a configuração do backend PHP.

Sendo assim, acesse a pasta 'backend'. Nela, seguiremos os seguintes passos:

1. Utilizando algum banco de dados relacional, como MySQL, criaremos uma nova database, de preferência na codificação 'utf8'.

2. Feito isso, copiaremos o arquivo **.env.example** sob o nome de **.env**.

    ***Obs:** Podemos fazer esse mesmo passo digitando o seguinte comando no terminal:*
    ``` bash
    $ cp .env.example .env
    ```

    Dentro deste arquivo, serão setadas as informações de usuário, host, senha e demais configurações que envolvam o banco de dados.

3. Com o banco criado e retornando à pasta backend, rodaremos os seguintes comandos através do terminal:
    ``` bash
    $ php artisan key:generate
    ```

    ``` bash
    $ php artisan migrate:fresh --seed
    ```

    ``` bash
    $ php artisan passport:install
    ```
## Uso
Após a instalação e configuração, serviremos o projeto em cada uma das pastas.

**Frontend Mobile (*pasta 'frontend'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ ionic serve
```

Para visualizar o frontend mobile, basta acessar [http://localhost:8100/](http://localhost:8100/) pelo seu navegador.

**Frontend Web (*pasta 'adminWeb'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ ng serve
```

Para visualizar o frontend web, basta acessar [http://localhost:4200/](http://localhost:4200/) pelo seu navegador.

**Backend PHP (*pasta 'backend'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:

``` bash
$ php artisan serve
```

Para visualizar o backend PHP, basta acessar [http://localhost:8000/](http://localhost:8000/).

**Backend Node (*pasta 'ethereum'*):**

Nesta pasta, basta rodar o seguinte comando através do terminal:
``` bash
$ npm start
```

## Arquitetura

Nos itens abaixo, temos acesso a documentos e ferramentas que foram importantes para a arquitetura e desenvolvimento do projeto.

- Documentação
- Trello do projeto
- Protótipo
- Modelagem de dados inicial (Disponível em breve)

## Autores

> **Período:** Desenvolvimento
* Gerente - Fulano
* Tech Lead - Fulano
* Dev Front-end - Fulano
* Dev Front-end - Fulano
* Dev Front-end - Fulano
* Dev Back-end - Fulano
* Dev Full Stack - Fulano
&nbsp;

> **Período:** Suporte
* Gerente - Fulano
* Tech Lead - Fulano
* Dev Front-end - Fulano
* Dev Front-end - Fulano
* Dev Back-end - Fulano
 


